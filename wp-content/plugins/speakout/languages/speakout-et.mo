��    g      T  �   �      �     �     �     �  G   �     $	     @	     D	     H	     N	     V	     [	     c	  	   q	     {	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     )
     0
     4
     9
     ?
     M
  
   `
     k
     y
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
     �
               	                      	   (     2     :     T  
   f     q     v     �     �  
   �     �     �     �     �     �     �     �       	     "        @     F  	   L     V  	   Z     d     m     s     �     �     �     �     �     �  
   �     �     �     �  4   �     (  �  7     7     G     `  H   i     �     �     �  	   �     �     �     �     �  
                   2     ;     @     P     X     l     �     �  
   �     �     �     �     �     �     �     �          !     /     =     S     Y     a     j     s     x     {     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �  
   �  
   �  
   	          /     <     T     Y  	   m     w  	   ~     �     �     �     �     �     �     �     �               (     .     7     E  
   I     T     ]  "   f     �     �     �     �     �     �  	   �     �  
   �     �  1   �     )        e   *   <      +   	   ^       ?       #   2   M          H   9   @   3       J           g   )   B               T   8   A               
   '   K   U   /   S   %                     $                ;   1      6   D   >       "           P   I       [      Q   =   c   R   .                               0   4   ]   X                 \         (      Y   _   F          ,   O   -   5      Z   E          d           f      7   b          !                W   G   a          C   &       N       V   L   `   :    Accepted tags: Accepted variables: Add New Add a custom field to the petition form for collecting additional data. Add me to your mailing list Apr Aug Basic Checked City Columns Confirm Email Confirmed Country Custom Message Date Dec Default Delete Display BCC field Display address fields Display custom field Display custom message Display honorific Display honorific field Donate Dr. Edit Email Email Address Email Confirmation Email From Email Message End date Enter title here Feb First Name Goal Greeting Hello ID Jan Jul Jun Label Language Last Name Mar May Message Miss Mr Mrs Ms Mx. Name No No Title None Nov Oct Petition Petition  Privacy Read or edit the petition Read the petition Return URL Rows Save Changes Sep Settings Signatures SpeakOut! Email Petitions SpeakOut! Petitions Settings State Street Street Address Success Message Support Target Email Thank you Thank you for signing our petition Theme Title Unchecked Yes confirmed disabled items long list (comma separated) no of of goal rows - default = 20 rows - default = 50 signature signatures signatures table use yes you may enter multiple addresses separated by commas your signature Project-Id-Version: SpeakOut! Email Petitions
POT-Creation-Date: 2018-02-22 10:56+1000
PO-Revision-Date: 2018-02-22 10:56+1000
Last-Translator: 123host <info@123host.com.au>
Language-Team: 
Language: et_EE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.0.6
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: speakout-email-petitions.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Aktsepteeritud: Aktsepteeritud muutujad: Lisa uus Kohandatud välja lisamine avalduse vormile täiendava teabe kogumiseks. Mind lisada oma meililistis Aprill August Põhiline Kontrollitud Linn Veerud Kinnitage e-post Kinnitatud Riik Kohandatud sõnum Kuupäev dets Vaikimisi valik Kustuta Näidake BCC välja Aadressi väljade kuvamine Kuva kohandatud välja Kuva kohandatud sõnum Kuva kraad Kuvatava austavaid välja Toeta Dr Muuda Email E-posti aadress E-posti kinnitamine Saatja e-post E-posti teade Lõppkuupäev Sisesta pealkiri siia veebr Eesnimi Eesmärk Tervitus Tere ID jaan Juuli Juuni Silt Keel Perekonnanimi Märts Mai Sõnum Prl Mr Pr Pr Mx. Nimi Ei Pealkiri puudub Puudub nov Oktoober Petitsioon Petitsioon Privaatsus Lugeda või muuta avalduse Loe avalduse Tagasipöördumise URL: Read Salvesta muudatused September Seaded Allkirjad SpeakOut! E-posti Petitsioonid SpeakOut! Petitsioonide seaded Maakond Tänav Tänav, maja Õnnestumise Teade Tugi Eesmärk e-posti Aitäh Täname meie avalduse Teema Pealkiri Kontrollimata Jah kinnitatud keelatud artiklid pikk nimekiri (komadega eraldatud) ei  /  eesmärk řidad - vaikimisi = 20 ridad - vaikimisi = 50 allkiri allkirju allkirjad laud kasutamine jah Võite sisestada mitu aadressi komadega eraldatud teie allkirjad 