��    y      �  �   �      8
     9
     H
     P
     ^
     b
     f
     l
     {
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
          /     D     [     m     �     �  +   �     �     �     �     �               -     3     A     T     b     r     �     �     �  
   �     �     �     �     �  *   �     �     �     �     �     �  	                                  $     '     +     0     3     <     A     E     I  	   R  �   \  T        Z     f     n     �     �     �     �     �     �     �  
   �     �  
             ,     I     O     `     g     v     �  	   �  4   �  9   �            	             !  	   @     J     S     Y     u     x     {     �     �     �  
   �     �     �     �     �  4   �       �  )     &     8     D     U     Y     ]     k     z     �     �     �  	   �     �     �     �     �     �     �     �     �     �     
     "     5     H     a     w     �     �  &   �     �     �                    ,     G     N     ]     o     }     �  
   �     �     �     �     �     �     �     �  +   �                         !  	   (     2     6     :     B     G     K     O     S     X     \     i     o     s     w  	   �  �   �  S   :  
   �  
   �     �     �     �     �     �     �     �  $     	   ;     E     W     `  "   s     �     �     �     �     �     �     �  8   �  *        E     J     Q     `     c  	   }  
   �     �     �     �     �     �     �     �     �  
   
       
     
   %     0  ,   3     `         q                   W       H      i          e      m   C      Y   	   p   4           ;                  n   R   <      8   S   )      V           E   >   @   "         7               h          2   -       (   f      6   '   $   X   D   b   c   r   5                  u   .   ^       K   F         O   G       T   !   ]   Z       v           x   L   [   3   &       s   N              I             l   0   
          P             B   y   d         /   t   J   k   :              `          w       =   ?   ,   M       _   1   a          \   9   j       *          %      g       U   +   #   o          Q   A    Accepted tags: Add New All Petitions Apr Aug Basic Call to Action Checked City Columns Confirm Email Confirmed Country Custom Field Date Dec Default Delete Display Display BCC field Display Options Display a petition form. Display address fields Display custom field Display custom message Display honorific Display honorific field Display opt-in checkbox Display signature count Do not send email (only collect signatures) Donate Download as CSV Dr. Edit Edit Email Petition Edit New Email Petition Email Email Address Email Confirmation Email Message Email Petitions Email Subject End date Enter title here Feb First Name Goal Greeting Hello ID Import petitions from SpeakUp to SpeakOut! Jan Jul Jun Label Language Last Name Mar May Miss Mr Mrs Ms Mx. Name No No Title None Nov Oct Petition Petition  Petition created. Use %1$s %2$s %3$s to display in a page or post. Use %1$s %4$s %3$s to display the signatures list or %1$s %5$s %3$s to show just the signature count. Please confirm your email address by clicking or copying and pasting the link below: Postal Code Privacy Read or edit the petition Read the petition Rows Save Changes Sep Settings Settings updated. Share this with your friends: Shortcodes Signature deleted. Signatures SpeakOut! Email Petitions SpeakOut! Petitions Settings State State / Province Street Street Address Success Message Support Thank you Thank you for signing our petition %petition_title%  Thank you. Your signature has been added to the petition. Theme Title Unchecked Yes Your signature has been added. confirmed disabled items long list (comma separated) no of of goal rows - default = 20 rows - default = 50 signature signatures signatures table unconfirmed use yes you may enter multiple addresses separated by commas your signature Project-Id-Version: SpeakOut! Email Petitions
POT-Creation-Date: 2018-02-22 11:01+1000
PO-Revision-Date: 2018-02-22 11:02+1000
Last-Translator: 123host <info@123host.com.au>
Language-Team: 
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.0.6
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: speakout-email-petitions.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Godkjente tagger: Legg til ny Aksepterte koder Apr Aug Grunnleggende Call to Action Valgt By Kolonner Bekreft din e-post Bekreftet Land Egendefinert felt Dato Des Standard Slett Skjerm Vise Blindkopi-feltet Visningsalternativer Vis et petisjonsskjema. Vis adressefeltene Vis tilpasset felt Vis egendefinert melding Skjermen Hederstittel Honorific visningsfeltet Vis avkrysningsruten for opt-in Visnings signatur teller Ikke send epost (kun samle signaturer) Donere Last ned som CSV Dr. Rediger Redigere Email-Petition Redigere ny Email-Petition E-post E-post adresse E-postbekreftelse E-postmelding E-postfordringer E-post emne Stopp Dato Skriv inn tittel her Feb Fornavn Målsettning Hilsen Hallo ID Importer konkurser fra SpeakUp å SpeakOut! Jan Jul Jun Merke Språk Etternavn Mar Mai Frøken Herr Fru Fr. Mx. Navn Nei Ingen Tittel Ingen Nov Okt Petisjon Petisjon  Opprop opprettet. Bruk %1$s %2$s %3$s til å vise i en side eller post. Bruk %1$s %4$s %3$s for å vise signaturlisten eller %1$s %5$s %3$s for å vise bare signaturtellingen. Bekreft e-postadressen din ved å klikke eller kopiere og lime inn linken nedenfor: Postnummer Personvern Lese og redigere begjæringen Lese begjæringen Rader Lagre Endringer Sept Innstillinger Innstillinger oppdatert. Del dette innlegget med dine venner: Kortlenke Signatur slettet. Signatur SpeakOut! E Opprop SpeakOut! Petisjoner innstillinger Fylke Delstat/Provins Gateadresse Gateadresse Suksess melding Support Takk Takk for at du signerte vår petisjon %petition_title%   Takk. Signaturen er lagt til begjæringen. Tema Tittel Ikke avkrysset Ja Din signatur er lagt til. bekreftet deaktivert artikler lang liste (kommaseparert) nei av målet rader - standard = 20 rader - standard = 50 underskrift underskrifter signaturer bord ubekreftet anvendelse ja Du kan angi flere adresser atskilt med komma din signatur 